#include <CapacitiveSensor.h>

/*
 * CapitiveSense Library Demo Sketch
 * Paul Badger 2008
 * modified sketch to include led
 */


CapacitiveSensor   cs_4_2 = CapacitiveSensor(4,2);        // 10M resistor between pins 4 & 2,
                                                          //  pin 2 is sensor pin 
                                                          //and to it I attached jelly

int const pinLed = 12;
void setup()                    
{
  pinMode (pinLed,OUTPUT);

  
   cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF);      // turn off autocalibrate on channel 1 
                                                  //- just as an example
   Serial.begin(9600);
}

void loop()                    
{
    long start = millis();
    long total1 =  cs_4_2.capacitiveSensor(30);
    if (total1 >80) { digitalWrite (pinLed,HIGH);}       // put value here from
                                                        //your sensor above which led should glow
    else { digitalWrite (pinLed,LOW); }
   

    Serial.println(total1);                  // print sensor output 1
   

    delay(50);                        
}
